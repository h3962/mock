let data = [
    {
        "id" : "0",
        "partner_name" : "Zimmer Biomet Deutschland",
        "partner_contact" : "Christoph Winzer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "1",
        "partner_name" : "]init[",
        "partner_contact" : "Marc Augsburg",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "2",
        "partner_name" : "]init[",
        "partner_contact" : "Alexander Reuss",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "3",
        "partner_name" : "11A | HR Vendor Management System",
        "partner_contact" : "Sebastian Steinkönig",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "4",
        "partner_name" : "11A | HR Vendor Management System",
        "partner_contact" : "Andreas Wittmann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "5",
        "partner_name" : "11A. HR VENDOR MANAGEMENT SYSTEM",
        "partner_contact" : "Marc Kilian",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "6",
        "partner_name" : "A. Stein´sche Buchhandlung",
        "partner_contact" : "Michael Borgers",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "7",
        "partner_name" : "A. Stein´sche Buchhandlung",
        "partner_contact" : "Gerrit van der Meer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "8",
        "partner_name" : "Actemium Energy Project",
        "partner_contact" : "Carsten Dürr",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "9",
        "partner_name" : "ADA Möbelwerke",
        "partner_contact" : "Martin Schinnerl",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "10",
        "partner_name" : "Airbus Operations",
        "partner_contact" : "Nils Witt",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "11",
        "partner_name" : "ALBA Group",
        "partner_contact" : "Kai Berking",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "12",
        "partner_name" : "ALFRED TALKE",
        "partner_contact" : "Natalie Hinz",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "13",
        "partner_name" : "ALFRED TALKE",
        "partner_contact" : "Joram Rosenthal",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "14",
        "partner_name" : "Alliance Healthcare Deutschland",
        "partner_contact" : "Vesna Bosnic-Wehner",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "15",
        "partner_name" : "Alliance Healthcare Deutschland",
        "partner_contact" : "Marco Schweizer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "16",
        "partner_name" : "ALTANA Management Services",
        "partner_contact" : "Joerg Mahn",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "17",
        "partner_name" : "Amazon",
        "partner_contact" : "Charlotte Anabelle de Brabandt",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "18",
        "partner_name" : "apetito",
        "partner_contact" : "Michel Bruning",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "19",
        "partner_name" : "apetito",
        "partner_contact" : "Larissa Klesper",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "20",
        "partner_name" : "apetito",
        "partner_contact" : "Christin Solke",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "21",
        "partner_name" : "APS Group",
        "partner_contact" : "Jürgen Kellner",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "22",
        "partner_name" : "ATH Altonaer-Technologie-Holding",
        "partner_contact" : "Dirk Naundorf",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "23",
        "partner_name" : "Aurubis",
        "partner_contact" : "Jörg Knips",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "24",
        "partner_name" : "Avatar Engines",
        "partner_contact" : "Dirk Schulte",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "25",
        "partner_name" : "AXA Assistance",
        "partner_contact" : "Ulrich Enke",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "26",
        "partner_name" : "AXA Konzern",
        "partner_contact" : "Lars Sommnitz",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "27",
        "partner_name" : "Axel Springer",
        "partner_contact" : "Marc Kaprál",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "28",
        "partner_name" : "B. Braun",
        "partner_contact" : "Anna-Maria Blum",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "29",
        "partner_name" : "B. Braun",
        "partner_contact" : "Dorothee Haack",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "30",
        "partner_name" : "B. Braun",
        "partner_contact" : "Markus Schnaudt",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "31",
        "partner_name" : "B. Metzler seel. Sohn",
        "partner_contact" : "Dirk Schreiber",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "32",
        "partner_name" : "BAD",
        "partner_contact" : "Andreas Rehbach",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "33",
        "partner_name" : "Balluff",
        "partner_contact" : "Edit Klein",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "34",
        "partner_name" : "Baloise Group",
        "partner_contact" : "Denis A. Bucher",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "35",
        "partner_name" : "BASF SE",
        "partner_contact" : "Philipp J. Böckmann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "36",
        "partner_name" : "Basler",
        "partner_contact" : "Nadine Haverkamp",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "37",
        "partner_name" : "Basler Versicherungen",
        "partner_contact" : "Denis A. Bucher",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "38",
        "partner_name" : "Basler Versicherungen",
        "partner_contact" : "Jonas Scherrer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "39",
        "partner_name" : "BEAM",
        "partner_contact" : "Andres Ruiz",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "40",
        "partner_name" : "BearingPoint",
        "partner_contact" : "Albert Keller",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "41",
        "partner_name" : "Bebecon s.r.o.",
        "partner_contact" : "Barbara Brabcova",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "42",
        "partner_name" : "Beeline",
        "partner_contact" : "Oleksandra Marx",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "43",
        "partner_name" : "Beiersdorf",
        "partner_contact" : "Matthias Bechtel",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "44",
        "partner_name" : "Beiersdorf",
        "partner_contact" : "Anne Bruns",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "45",
        "partner_name" : "Beiersdorf",
        "partner_contact" : "Isabel Hochgesand",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "46",
        "partner_name" : "Beiersdorf",
        "partner_contact" : "Henning Hofmann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "47",
        "partner_name" : "Beiersdorf",
        "partner_contact" : "Dr. Jörg Küther",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "48",
        "partner_name" : "BeNeering",
        "partner_contact" : "Christoph Moll",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "49",
        "partner_name" : "BeNeering",
        "partner_contact" : "Arne Schmidt",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "50",
        "partner_name" : "BeNeering",
        "partner_contact" : "Martin Schmitz",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "51",
        "partner_name" : "Berliner Verkehrsunternehmen",
        "partner_contact" : "Andreas Smarsly",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "52",
        "partner_name" : "Beutlhauser Holding",
        "partner_contact" : "Marko Jakob",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "53",
        "partner_name" : "Bizerba",
        "partner_contact" : "Frank Osterhagen",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "54",
        "partner_name" : "Böcker Beratung",
        "partner_contact" : "Ulrike Böcker",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "55",
        "partner_name" : "Boeing Distribution Services",
        "partner_contact" : "Sebastian Göbel",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "56",
        "partner_name" : "Bosch",
        "partner_contact" : "Ehab Abdelaziz",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "57",
        "partner_name" : "Bosch",
        "partner_contact" : "Harry Schorsch",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "58",
        "partner_name" : "BS|ENERGY",
        "partner_contact" : "Adrian Zuber",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "59",
        "partner_name" : "Burda Procurement",
        "partner_contact" : "Patrick Gressung",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "60",
        "partner_name" : "Burda Procurement",
        "partner_contact" : "Lydia Otto",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "61",
        "partner_name" : "Burda Procurement",
        "partner_contact" : "Daniela Vorbauer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "62",
        "partner_name" : "BuyIn",
        "partner_contact" : "Sven Meyer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "63",
        "partner_name" : "Capgemini Germany",
        "partner_contact" : "Kai Hasenklever",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "64",
        "partner_name" : "Carl Kühne",
        "partner_contact" : "Kay Preuß",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "65",
        "partner_name" : "China Civil Engineering Construction Corporation",
        "partner_contact" : "Morshedul Islam",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "66",
        "partner_name" : "China Civil Engineering Construction Corporation",
        "partner_contact" : "Morshedul Islam",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "67",
        "partner_name" : "China Civil Engineering Construction Corporation",
        "partner_contact" : "Morshedul Islam",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "68",
        "partner_name" : "Christian Thöne Managementconsulting",
        "partner_contact" : "Christian Thöne",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "69",
        "partner_name" : "Coca-Cola",
        "partner_contact" : "Markus Wind",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "70",
        "partner_name" : "Coloplast",
        "partner_contact" : "Jörn Kramer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "71",
        "partner_name" : "Columbus UK Holding",
        "partner_contact" : "Mareike Ahner",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "72",
        "partner_name" : "Competec Service",
        "partner_contact" : "Patrick Itel",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "73",
        "partner_name" : "Competec Service",
        "partner_contact" : "Philipp Schelb",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "74",
        "partner_name" : "compleet",
        "partner_contact" : "Alexander Sadek",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "75",
        "partner_name" : "Conrad",
        "partner_contact" : "Holm Lehmann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "76",
        "partner_name" : "Constantia Flexibles Group",
        "partner_contact" : "Roland Schwoegler",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "77",
        "partner_name" : "Coupa Deutschland",
        "partner_contact" : "Ben Grudda",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "78",
        "partner_name" : "Creactives",
        "partner_contact" : "Stephan Willigens",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "79",
        "partner_name" : "Crowdfox",
        "partner_contact" : "Dirk Schäfer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "80",
        "partner_name" : "CSL Behring",
        "partner_contact" : "Oliver Hahn",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "81",
        "partner_name" : "CureVac",
        "partner_contact" : "Romina Schirmeisen",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "82",
        "partner_name" : "d&b audiotechnik",
        "partner_contact" : "Michaela Spichtinger",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "83",
        "partner_name" : "DaVita Deutschland",
        "partner_contact" : "Frauke Laubsch",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "84",
        "partner_name" : "DB Management Consulting",
        "partner_contact" : "Manuel Kapfer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "85",
        "partner_name" : "DeepStream Technologies",
        "partner_contact" : "Sam  Crow",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "86",
        "partner_name" : "Deutsche Apotheker- und Ärztebank",
        "partner_contact" : "Sebastian Jungmann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "87",
        "partner_name" : "DB Schenker",
        "partner_contact" : "Dr. Ralf Erhardt",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "88",
        "partner_name" : "DIHUG - hülsta-werke Hüls",
        "partner_contact" : "Stephan Knuth",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "89",
        "partner_name" : "Doka",
        "partner_contact" : "Thomas Zsulits",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "90",
        "partner_name" : "DOYMA",
        "partner_contact" : "Saskia Meyer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "91",
        "partner_name" : "dSPACE",
        "partner_contact" : "Stefan Tölle",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "92",
        "partner_name" : "Düllberg Konzentra",
        "partner_contact" : "Kathrin Thiesen",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "93",
        "partner_name" : "DURAG GROUP",
        "partner_contact" : "Markus Holz-Wollenzin",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "94",
        "partner_name" : "Dürr Systems",
        "partner_contact" : "Andre Schommer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "95",
        "partner_name" : "Dürr Systems",
        "partner_contact" : "Marcel Wolfer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "96",
        "partner_name" : "Durch Denken Nach Vorne Consult",
        "partner_contact" : "Frank  Sundermann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "97",
        "partner_name" : "Easy2Parts",
        "partner_contact" : "Sebastian Freund",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "98",
        "partner_name" : "Easy2Parts",
        "partner_contact" : "Finn Wulbrand",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "99",
        "partner_name" : "ecoworks",
        "partner_contact" : "Maike Dust",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "100",
        "partner_name" : "edding international",
        "partner_contact" : "Nicolas Nohr",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "101",
        "partner_name" : "Einkaufsinitiative für die Sozialwirtschaft",
        "partner_contact" : "Gabriele Heller",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "102",
        "partner_name" : "EINWERK",
        "partner_contact" : "Heiner Baerecke",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "103",
        "partner_name" : "EINWERK",
        "partner_contact" : "Berit Vider",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "104",
        "partner_name" : "EMBL",
        "partner_contact" : "Evelyne Cudraz",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "105",
        "partner_name" : "Emma Sleep",
        "partner_contact" : "Pia Kleiber",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "106",
        "partner_name" : "EnBW",
        "partner_contact" : "Corinna Halla",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "107",
        "partner_name" : "EnBW",
        "partner_contact" : "Frank Pottiez",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "108",
        "partner_name" : "EnBW",
        "partner_contact" : "Stefan Wagner",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "109",
        "partner_name" : "EnBW Energie Baden-Württemberg",
        "partner_contact" : "Jürgen Schuster",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "110",
        "partner_name" : "Energiedienst-Gruppe",
        "partner_contact" : "Sven Goronzi",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "111",
        "partner_name" : "Eqip",
        "partner_contact" : "Kristina Belka",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "112",
        "partner_name" : "Eqip",
        "partner_contact" : "Daniel Gross",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "113",
        "partner_name" : "Eqip",
        "partner_contact" : "Tobias Schmidt",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "114",
        "partner_name" : "ERGO",
        "partner_contact" : "Dr.-Ing. Thomas Schneider",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "115",
        "partner_name" : "Europa Green",
        "partner_contact" : "Friederike Quast",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "116",
        "partner_name" : "Eventic",
        "partner_contact" : "Paul Philipp Hermann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "117",
        "partner_name" : "Evonik Operations",
        "partner_contact" : "Elmar Mitterreiter (Dr.)",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "118",
        "partner_name" : "fischerwerke",
        "partner_contact" : "Kay Rogge",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "119",
        "partner_name" : "Flottweg",
        "partner_contact" : "Stefan Windschiegl",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "120",
        "partner_name" : "Flowciety",
        "partner_contact" : "Tim Kiese",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "121",
        "partner_name" : "Flowciety",
        "partner_contact" : "Dr. Thomas Kintscher",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "122",
        "partner_name" : "FLUX-GERÄTE",
        "partner_contact" : "Alexander Geib",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "123",
        "partner_name" : "FLUX-GERÄTE",
        "partner_contact" : "Michael Uhlich",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "124",
        "partner_name" : "Fraunhofer-Gesellschaft",
        "partner_contact" : "Andreas Kannt",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "125",
        "partner_name" : "FUNKE Mediengruppe",
        "partner_contact" : "Tim Schroeer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "126",
        "partner_name" : "FUNKE Mediengruppe",
        "partner_contact" : "Moritz Vogt",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "127",
        "partner_name" : "General Services Switzerland",
        "partner_contact" : "Joerg Bortoluzzi",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "128",
        "partner_name" : "Gerolsteiner",
        "partner_contact" : "Marcus Schumacher",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "129",
        "partner_name" : "GOK Regler- und Armaturen",
        "partner_contact" : "Joshua Weißenberger",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "130",
        "partner_name" : "Gollnest & Kiesel",
        "partner_contact" : "Marcel Lindemann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "131",
        "partner_name" : "Google",
        "partner_contact" : "Jason Nagle",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "132",
        "partner_name" : "Gorillas",
        "partner_contact" : "Seif El-Sobky",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "133",
        "partner_name" : "Gorillas Operations Germany",
        "partner_contact" : "Marthe Geulette",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "134",
        "partner_name" : "GROB-WERKE",
        "partner_contact" : "Gerd Hofmann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "135",
        "partner_name" : "GULP",
        "partner_contact" : "Harald Muth",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "136",
        "partner_name" : "H&F Solutions",
        "partner_contact" : "Tobias Hertfelder",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "137",
        "partner_name" : "H&F Solutions",
        "partner_contact" : "Valerie Lee",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "138",
        "partner_name" : "Hamburger Hochbahn",
        "partner_contact" : "Boris Brauner",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "139",
        "partner_name" : "Hamburger Sparkasse",
        "partner_contact" : "Tanja Punnanchira",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "140",
        "partner_name" : "Hamburger Sparkasse",
        "partner_contact" : "Miriam Tomforde",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "141",
        "partner_name" : "HANSA Maschinenbau Vertriebs- und Fertigung",
        "partner_contact" : "Maik Schnibbe",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "142",
        "partner_name" : "HANSA-FLEX",
        "partner_contact" : "Merle Barta",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "143",
        "partner_name" : "HANSA-FLEX",
        "partner_contact" : "Edwin Maringka",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "144",
        "partner_name" : "Hapag-Lloyd",
        "partner_contact" : "Eileen  Peters",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "145",
        "partner_name" : "Hapag-Lloyd",
        "partner_contact" : "Benita Niemann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "146",
        "partner_name" : "Hapag-Lloyd",
        "partner_contact" : "Alison Tonja  Nauer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "147",
        "partner_name" : "HDS Consulting",
        "partner_contact" : "Guido Händeler",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "148",
        "partner_name" : "Helaba",
        "partner_contact" : "Lucas Gottschalk",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "149",
        "partner_name" : "Helmholtz-Zentrum Hereon",
        "partner_contact" : "Dirk Jordan",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "150",
        "partner_name" : "Helvetia Versicherungen",
        "partner_contact" : "Andreas Schulz",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "151",
        "partner_name" : "HERMA",
        "partner_contact" : "Tobias Demmer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "152",
        "partner_name" : "Hermes Schleifmittel",
        "partner_contact" : "Sven Wolckenhauer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "153",
        "partner_name" : "Hermes Schleifmittel",
        "partner_contact" : "Sven Wolckenhauer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "154",
        "partner_name" : "HIL Heeresinstandsetzungslogistik",
        "partner_contact" : "Lydia Möhnen",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "155",
        "partner_name" : "HIL Heeresinstandsetzungslogistik",
        "partner_contact" : "Sandro Zambito",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "156",
        "partner_name" : "Hivebuy",
        "partner_contact" : "Bettina Fischer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "157",
        "partner_name" : "Hivebuy",
        "partner_contact" : "Christopher Sorg",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "158",
        "partner_name" : "Hochschule für Angewandte Wissenschaften München",
        "partner_contact" : "Prof. Dr. Florian C. Kleemann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "159",
        "partner_name" : "HORNBACH Baumarkt",
        "partner_contact" : "Gunther Löhlein",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "160",
        "partner_name" : "HOYER",
        "partner_contact" : "Matthias Synal",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "161",
        "partner_name" : "Hubert Burda Media / Burda Procurement",
        "partner_contact" : "Florian Falk",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "162",
        "partner_name" : "Ignite Procurement",
        "partner_contact" : "Prisca Steiner",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "163",
        "partner_name" : "INEOS Automotive",
        "partner_contact" : "Oliver Frille",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "164",
        "partner_name" : "INHUBBER",
        "partner_contact" : "Dr. Elena Mechik",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "165",
        "partner_name" : "Innolytics",
        "partner_contact" : "Dr. Jens-Uwe Meyer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "166",
        "partner_name" : "Integra Internet Management",
        "partner_contact" : "Thomas Behrens",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "167",
        "partner_name" : "Interim Manager & Coach",
        "partner_contact" : "Stefanie Henko",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "168",
        "partner_name" : "IU Internationale Hochschule",
        "partner_contact" : "Matthias Knoppik",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "169",
        "partner_name" : "JAGGAER",
        "partner_contact" : "Klaus Boos",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "170",
        "partner_name" : "JAGGAER",
        "partner_contact" : "Marco Freund",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "171",
        "partner_name" : "JAGGAER",
        "partner_contact" : "Jochen Krüger",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "172",
        "partner_name" : "JAGGAER",
        "partner_contact" : "Fabio Obwexer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "173",
        "partner_name" : "JAGGAER",
        "partner_contact" : "Michael Quack",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "174",
        "partner_name" : "JAGGAER",
        "partner_contact" : "Oleksandra Sydorenko",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "175",
        "partner_name" : "JAGGAER",
        "partner_contact" : "Ulrich Ulrich",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "176",
        "partner_name" : "James Meads Media & Consulting",
        "partner_contact" : "James Meads",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "177",
        "partner_name" : "Jakob Müller",
        "partner_contact" : "Mario Bruggmann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "178",
        "partner_name" : "Janitza electronics",
        "partner_contact" : "Helmut Maier",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "179",
        "partner_name" : "Janitza electronics",
        "partner_contact" : "Martin Truthe",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "180",
        "partner_name" : "JKB-Consulting",
        "partner_contact" : "Jochen Bremshey",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "181",
        "partner_name" : "Journalist and Mdia Correspondent",
        "partner_contact" : "Udo Lielischkies",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "182",
        "partner_name" : "Joyson",
        "partner_contact" : "Velat Özkilinc",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "183",
        "partner_name" : "Jungheinrich",
        "partner_contact" : "Hans-Henning Hein",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "184",
        "partner_name" : "K A R L – GRUPPE",
        "partner_contact" : "Silvio Jakob",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "185",
        "partner_name" : "K+S AG",
        "partner_contact" : "Niels Walberg",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "186",
        "partner_name" : "Keelvar",
        "partner_contact" : "Christian Herdelt",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "187",
        "partner_name" : "Keelvar",
        "partner_contact" : "Manuel Ringle",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "188",
        "partner_name" : "Kerkhoff Consulting",
        "partner_contact" : "Sebastian Lindlahr",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "189",
        "partner_name" : "Kerkhoff Consulting",
        "partner_contact" : "Stephan Weaver",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "190",
        "partner_name" : "Klosterfrau Berlin",
        "partner_contact" : "Sandra Mangler",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "191",
        "partner_name" : "Klosterfrau Berlin",
        "partner_contact" : "Stefanie Mayr",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "192",
        "partner_name" : "Knauf Gips",
        "partner_contact" : "Dr. Michael Proch",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "193",
        "partner_name" : "Kodiak Hub",
        "partner_contact" : "Melanie Follonier",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "194",
        "partner_name" : "Kodiak Hub",
        "partner_contact" : "Daniel Meyer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "195",
        "partner_name" : "Kodiak Hub",
        "partner_contact" : "Achim Richarz",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "196",
        "partner_name" : "Kodiak Hub",
        "partner_contact" : "Sofia Sandström",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "197",
        "partner_name" : "Körber",
        "partner_contact" : "Michael Schürmann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "198",
        "partner_name" : "Körber",
        "partner_contact" : "Michael Stietz",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "199",
        "partner_name" : "Kroschke sign international",
        "partner_contact" : "Stefanie Steiner",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "200",
        "partner_name" : "Kühne + Nagel",
        "partner_contact" : "Manuel Plewe",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "201",
        "partner_name" : "kwb Germany",
        "partner_contact" : "Tim Ulrich",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "202",
        "partner_name" : "KWS Germany",
        "partner_contact" : "Guido vom Berge",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "203",
        "partner_name" : "KWS Germany",
        "partner_contact" : "David Ladwig",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "204",
        "partner_name" : "Leiber",
        "partner_contact" : "Rolf Warns",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "205",
        "partner_name" : "Lemon Learning",
        "partner_contact" : "Fynn Rauenschwender",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "206",
        "partner_name" : "LESER",
        "partner_contact" : "Lars Geruschke",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "207",
        "partner_name" : "LeverX",
        "partner_contact" : "Volha Chekun",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "208",
        "partner_name" : "LeverX",
        "partner_contact" : "Tomasz Gigon",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "209",
        "partner_name" : "LeverX",
        "partner_contact" : "Faig Guliyev",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "210",
        "partner_name" : "LeverX",
        "partner_contact" : "Yulia Haravik",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "211",
        "partner_name" : "LeverX",
        "partner_contact" : "Eleonora Kalinina",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "212",
        "partner_name" : "LeverX",
        "partner_contact" : "Irina Koritskaya",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "213",
        "partner_name" : "LeverX",
        "partner_contact" : "Kirill Rodionov",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "214",
        "partner_name" : "Lhotse Analytics",
        "partner_contact" : "Daniel Demuth",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "215",
        "partner_name" : "Lhotse Analytics",
        "partner_contact" : "Nicolas Neubauer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "216",
        "partner_name" : "Lhotse Analytics",
        "partner_contact" : "Dennis André Ollig",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "217",
        "partner_name" : "Liebherr-Components",
        "partner_contact" : "Richard Lienert",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "218",
        "partner_name" : "Lintum",
        "partner_contact" : "Marielle Gantenberg",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "219",
        "partner_name" : "Lintum",
        "partner_contact" : "Christian W. Jakob",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "220",
        "partner_name" : "Lintum",
        "partner_contact" : "Marina Miretskaya",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "221",
        "partner_name" : "Lintum",
        "partner_contact" : "Georgios Tsouvekis",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "222",
        "partner_name" : "LIS Data Solutions",
        "partner_contact" : "Asier Barredo",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "223",
        "partner_name" : "LIS Data Solutions",
        "partner_contact" : "Myriam Klatt",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "224",
        "partner_name" : "LIS Data Solutions",
        "partner_contact" : "Matthias Rothfuchs",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "225",
        "partner_name" : "Living Quarter",
        "partner_contact" : "Hanno Kamprüwen",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "226",
        "partner_name" : "LPP Lotao Pack-und Produktions",
        "partner_contact" : "Susanne Dietrich",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "227",
        "partner_name" : "Lufthansa Technik",
        "partner_contact" : "Lennart May",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "228",
        "partner_name" : "Mabanaft",
        "partner_contact" : "Andre Neumann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "229",
        "partner_name" : "Magna International",
        "partner_contact" : "Sabine Behrens",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "230",
        "partner_name" : "Marc Cain",
        "partner_contact" : "Mario Ruoff",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "231",
        "partner_name" : "marktIT",
        "partner_contact" : "Firat Yilmaz",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "232",
        "partner_name" : "Marley Deutschland",
        "partner_contact" : "Steffen Fitzpatrick",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "233",
        "partner_name" : "Matchory",
        "partner_contact" : "Aiko Wiegand",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "234",
        "partner_name" : "mb consulting partners",
        "partner_contact" : "Daniel Sorgler",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "235",
        "partner_name" : "MBI Martin Brückner Infosource",
        "partner_contact" : "Dr. Mark Krieger",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "236",
        "partner_name" : "MBition",
        "partner_contact" : "Florian Körner",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "237",
        "partner_name" : "MBition",
        "partner_contact" : "Viktoria Kufel",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "238",
        "partner_name" : "MBition",
        "partner_contact" : "Jan Nordenbrock",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "239",
        "partner_name" : "MBition",
        "partner_contact" : "Lutz Wagenführer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "240",
        "partner_name" : "medi",
        "partner_contact" : "Julia Blaseck",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "241",
        "partner_name" : "medi",
        "partner_contact" : "Marina Freyberger",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "242",
        "partner_name" : "MEKRA",
        "partner_contact" : "Oliver Sommer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "243",
        "partner_name" : "Mercanis",
        "partner_contact" : "Fabian Heinrich",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "244",
        "partner_name" : "Metroplan",
        "partner_contact" : "Dr. Thomas Mielke",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "245",
        "partner_name" : "Metroplan",
        "partner_contact" : "Andree Siever",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "246",
        "partner_name" : "Meyer Werft",
        "partner_contact" : "Alexander Höfling",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "247",
        "partner_name" : "MEYLE",
        "partner_contact" : "Sebastian Berndt",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "248",
        "partner_name" : "Microsoft",
        "partner_contact" : "Lydia Mamesah",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "249",
        "partner_name" : "Microsoft",
        "partner_contact" : "Peter Stock",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "250",
        "partner_name" : "Migros-Genossenschafts-Bund",
        "partner_contact" : "Julia Heisig",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "251",
        "partner_name" : "MOCOM Compounds",
        "partner_contact" : "Dr. Philipp Grote",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "252",
        "partner_name" : "MSD",
        "partner_contact" : "Polyvios Dimiou",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "253",
        "partner_name" : "NDR",
        "partner_contact" : "Manuela Haddadzadeh",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "254",
        "partner_name" : "Negotiation Advisory Group",
        "partner_contact" : "Yurda Burghardt",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "255",
        "partner_name" : "Negotiation Advisory Group",
        "partner_contact" : "René Schumann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "256",
        "partner_name" : "Negotiation Advisory Group",
        "partner_contact" : "Katharina Weber",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "257",
        "partner_name" : "Negotiation Advisory Group",
        "partner_contact" : "Katharina Weber",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "258",
        "partner_name" : "Negotiation Advisory Group",
        "partner_contact" : "Patricia Zimmermann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "259",
        "partner_name" : "Nestlé",
        "partner_contact" : "Gaby Symonds",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "260",
        "partner_name" : "Nestlé",
        "partner_contact" : "Florian Willems",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "261",
        "partner_name" : "Netive VMS B.V.",
        "partner_contact" : "Rolf Hamersma",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "262",
        "partner_name" : "Netstock Europe",
        "partner_contact" : "Jörg Salomo",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "263",
        "partner_name" : "Netzsch Feinmahltechnik",
        "partner_contact" : "Marta Zimoch",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "264",
        "partner_name" : "Neumann Kaffee Gruppe",
        "partner_contact" : "Julia Herrling",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "265",
        "partner_name" : "Neumann Kaffee Gruppe",
        "partner_contact" : "Katja Wilde",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "266",
        "partner_name" : "New Flag",
        "partner_contact" : "Bernd Kruschewski",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "267",
        "partner_name" : "Newtron",
        "partner_contact" : "Stefan Ernst",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "268",
        "partner_name" : "Newtron",
        "partner_contact" : "Kathleen Hirschfeld",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "269",
        "partner_name" : "Newtron",
        "partner_contact" : "Nataliia Prokopets",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "270",
        "partner_name" : "Newtron",
        "partner_contact" : "Jan Rößler",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "271",
        "partner_name" : "Newtron",
        "partner_contact" : "Patrick Schmiedehaus",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "272",
        "partner_name" : "Newtron",
        "partner_contact" : "Armin Thielemann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "273",
        "partner_name" : "New Work",
        "partner_contact" : "Julius Deselaers",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "274",
        "partner_name" : "Nolte Küchen",
        "partner_contact" : "Johannes Heitmann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "275",
        "partner_name" : "Nordex",
        "partner_contact" : "Sascha Holweg",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "276",
        "partner_name" : "NORDWEST Handel",
        "partner_contact" : "Claudia Preuß",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "277",
        "partner_name" : "NORDWEST Handel",
        "partner_contact" : "Stefan Richlick",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "278",
        "partner_name" : "Nordzucker",
        "partner_contact" : "Daniel Brown",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "279",
        "partner_name" : "Novartis Pharma AG",
        "partner_contact" : "Patricia Catuogno",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "280",
        "partner_name" : "NRW.BANK",
        "partner_contact" : "Janpeter Beckmann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "281",
        "partner_name" : "OCCON",
        "partner_contact" : "Joachim Stellner",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "282",
        "partner_name" : "OJC Consulting",
        "partner_contact" : "Nicolas Sevaux",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "283",
        "partner_name" : "Olin Germany Upstream",
        "partner_contact" : "Sascha Gnatenko",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "284",
        "partner_name" : "Olin Germany Upstream",
        "partner_contact" : "Robert Schramm",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "285",
        "partner_name" : "Olin Germany Upstream",
        "partner_contact" : "Tanja Schwenke",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "286",
        "partner_name" : "Olympus",
        "partner_contact" : "Vesselina Vassileva-Zlatev",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "287",
        "partner_name" : "Onventis",
        "partner_contact" : "Benjamin Fritz",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "288",
        "partner_name" : "Osram",
        "partner_contact" : "Uwe Kleinhof",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "289",
        "partner_name" : "Ottobock",
        "partner_contact" : "Nils Lennig",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "290",
        "partner_name" : "Österreichische Post",
        "partner_contact" : "Christina Kukla",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "291",
        "partner_name" : "P.S. Cooperation",
        "partner_contact" : "Michael Blaschke",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "292",
        "partner_name" : "P.S. Cooperation",
        "partner_contact" : "Josef Gutsmiedl",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "293",
        "partner_name" : "Pandora's Brain",
        "partner_contact" : "Calum Chace",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "294",
        "partner_name" : "parasus",
        "partner_contact" : "Hermann Ebert",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "295",
        "partner_name" : "parasus",
        "partner_contact" : "Daniel Kirchhübel",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "296",
        "partner_name" : "parasus",
        "partner_contact" : "Ralph Salokat",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "297",
        "partner_name" : "PartSpace",
        "partner_contact" : "Sebastian Freund",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "298",
        "partner_name" : "Paua Ventures",
        "partner_contact" : "Felix Plapperer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "299",
        "partner_name" : "Paxly",
        "partner_contact" : "Thomas Auer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "300",
        "partner_name" : "Paxly",
        "partner_contact" : "Sascha Möller",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "301",
        "partner_name" : "Paxly",
        "partner_contact" : "Linus Szagun",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "302",
        "partner_name" : "Per Angusta",
        "partner_contact" : "Marine Poincot",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "303",
        "partner_name" : "Per Angusta",
        "partner_contact" : "Frank Thewihsen",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "304",
        "partner_name" : "Per Angusta",
        "partner_contact" : "Susanne Truchet",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "305",
        "partner_name" : "perseus",
        "partner_contact" : "Thorsten Linge",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "306",
        "partner_name" : "PLEUGER industries",
        "partner_contact" : "Stefan Gagern",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "307",
        "partner_name" : "Polit-X",
        "partner_contact" : "Heiko Schnitzler",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "308",
        "partner_name" : "Porsche",
        "partner_contact" : "Thomas Pichler",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "309",
        "partner_name" : "Powerlines Group",
        "partner_contact" : "Michaela Gütl",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "310",
        "partner_name" : "PPG",
        "partner_contact" : "Birgit Stadie",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "311",
        "partner_name" : "prego services",
        "partner_contact" : "Claus Müller",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "312",
        "partner_name" : "Premium Aerotec",
        "partner_contact" : "Daniela Balzer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "313",
        "partner_name" : "prime-ing",
        "partner_contact" : "Markus Grüttner",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "314",
        "partner_name" : "prime-ing",
        "partner_contact" : "Jakob Jungmeier",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "315",
        "partner_name" : "prime-ing",
        "partner_contact" : "Christoph Sedlmeir",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "316",
        "partner_name" : "prime-ing",
        "partner_contact" : "Arno Singer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "317",
        "partner_name" : "PrintPlanet",
        "partner_contact" : "Bernhard Chr. Berger",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "318",
        "partner_name" : "PRO Ulimited Global Germany GmbH",
        "partner_contact" : "Simon Läpple",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "319",
        "partner_name" : "procure.ch",
        "partner_contact" : "Adrian Jungo",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "320",
        "partner_name" : "procure.ch",
        "partner_contact" : "Andreas Kyburz",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "321",
        "partner_name" : "Procurement Summit",
        "partner_contact" : "Till Dickomey",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "322",
        "partner_name" : "Procurement Summit",
        "partner_contact" : "Nele Flach",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "323",
        "partner_name" : "Procurement Summit",
        "partner_contact" : "Marie Kröger",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "324",
        "partner_name" : "Procurement Summit",
        "partner_contact" : "Paulina Krösche",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "325",
        "partner_name" : "Procuros",
        "partner_contact" : "Patrick Thelen",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "326",
        "partner_name" : "Prospeum",
        "partner_contact" : "Friedrich Kern",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "327",
        "partner_name" : "PSG Procurement Services",
        "partner_contact" : "René Stromberg",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "328",
        "partner_name" : "QAD Europe",
        "partner_contact" : "Bernhard Soltmann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "329",
        "partner_name" : "R+V Allgemeine Versicherung",
        "partner_contact" : "Kim Kuhlen",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "330",
        "partner_name" : "Rajapack",
        "partner_contact" : "Kristoffer Kicherer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "331",
        "partner_name" : "Rajapack",
        "partner_contact" : "Fabian Kraft",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "332",
        "partner_name" : "Randstad Sourceright",
        "partner_contact" : "Daniela Derksen",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "333",
        "partner_name" : "Randstad Sourceright",
        "partner_contact" : "Christian Neuerburg",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "334",
        "partner_name" : "Randstad Sourceright",
        "partner_contact" : "Tatiana Ohm",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "335",
        "partner_name" : "Randstad Sourceright",
        "partner_contact" : "Ben Brierley",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "336",
        "partner_name" : "Randstad Sourceright",
        "partner_contact" : "Katharina Kühn",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "337",
        "partner_name" : "Rational",
        "partner_contact" : "Sebastian Gaschler",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "338",
        "partner_name" : "RheinEnergie",
        "partner_contact" : "Ralf Seehase",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "339",
        "partner_name" : "Robert Bosch",
        "partner_contact" : "Evgenii Astapov",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "340",
        "partner_name" : "RS Components",
        "partner_contact" : "Bettina Staude",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "341",
        "partner_name" : "RTG Retail Trade Group",
        "partner_contact" : "Marcel Nowitzki",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "342",
        "partner_name" : "RWE Renewables",
        "partner_contact" : "Michael Veeser",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "343",
        "partner_name" : "Salamander Industrie-Produkte",
        "partner_contact" : "Karin Miller",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "344",
        "partner_name" : "SAP",
        "partner_contact" : "Achim Birk",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "345",
        "partner_name" : "SAP",
        "partner_contact" : "Patrick Schiller",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "346",
        "partner_name" : "Sartorius",
        "partner_contact" : "Tobias Pohl",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "347",
        "partner_name" : "Sartorius",
        "partner_contact" : "Steffen Schlieker",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "348",
        "partner_name" : "Sartorius",
        "partner_contact" : "Marco Senhen",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "349",
        "partner_name" : "Schweitzer Fachinformationen",
        "partner_contact" : "Julia Christiansen",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "350",
        "partner_name" : "Schweitzer Fachinformationen",
        "partner_contact" : "Alexander Graff",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "351",
        "partner_name" : "Schweitzer Fachinformationen",
        "partner_contact" : "Annette Jagoda",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "352",
        "partner_name" : "Schweitzer Fachinformationen",
        "partner_contact" : "Roxane Lämmermann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "353",
        "partner_name" : "Schweitzer Fachinformationen",
        "partner_contact" : "Franziska Lang",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "354",
        "partner_name" : "Schweitzer Fachinformationen",
        "partner_contact" : "Hendrik Michel",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "355",
        "partner_name" : "Schweitzer Fachinformationen",
        "partner_contact" : "Marius Neuschl",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "356",
        "partner_name" : "SEG Automotive",
        "partner_contact" : "Nina Bomberg",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "357",
        "partner_name" : "ShareNow",
        "partner_contact" : "Anastasios Aslanidis",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "358",
        "partner_name" : "ShareNow",
        "partner_contact" : "Sina Niemann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "359",
        "partner_name" : "ShareNow",
        "partner_contact" : "Jan Schröder",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "360",
        "partner_name" : "ShareNow",
        "partner_contact" : "Tatjana Wetzstein",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "361",
        "partner_name" : "SHIPSTA",
        "partner_contact" : "Sophie Cunningham",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "362",
        "partner_name" : "SHIPSTA",
        "partner_contact" : "Oliver Esch",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "363",
        "partner_name" : "SHIPSTA",
        "partner_contact" : "Arjan Jager",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "364",
        "partner_name" : "SHIPSTA",
        "partner_contact" : "Stephen Power",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "365",
        "partner_name" : "SHIPSTA",
        "partner_contact" : "Oliver Ritzmann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "366",
        "partner_name" : "Siemens",
        "partner_contact" : "André Sponsel",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "367",
        "partner_name" : "Siemens Energy",
        "partner_contact" : "Henriette Adams",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "368",
        "partner_name" : "Siemens Energy",
        "partner_contact" : "Christoph Wehinger",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "369",
        "partner_name" : "Silicon Austria Labs",
        "partner_contact" : "Barbara Fuka-Viola",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "370",
        "partner_name" : "Sixt",
        "partner_contact" : "Marcel Fritsch",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "371",
        "partner_name" : "SIXT",
        "partner_contact" : "Ingo Rietdorf",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "372",
        "partner_name" : "Sky",
        "partner_contact" : "Alexander Pingert",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "373",
        "partner_name" : "Sonepar Deutschland",
        "partner_contact" : "Matthias Soller",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "374",
        "partner_name" : "Sopra Steria",
        "partner_contact" : "Marco Wagner",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "375",
        "partner_name" : "Sourcing Heads GmbH",
        "partner_contact" : "Wladimir Huwa",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "376",
        "partner_name" : "stadtwerke Solingen",
        "partner_contact" : "Christian Schneider",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "377",
        "partner_name" : "Steinbeis Papier",
        "partner_contact" : "Nico Bielefeldt",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "378",
        "partner_name" : "Steinbeis Papier",
        "partner_contact" : "Torben Link",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "379",
        "partner_name" : "Strabag",
        "partner_contact" : "Germo Schroebler",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "380",
        "partner_name" : "SV Werder Bremen",
        "partner_contact" : "Sebastian Janzen",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "381",
        "partner_name" : "Swiss Post",
        "partner_contact" : "Astrid Borgmann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "382",
        "partner_name" : "symex",
        "partner_contact" : "Natalia Betke",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "383",
        "partner_name" : "symex",
        "partner_contact" : "Mandy Gehrmann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "384",
        "partner_name" : "Tchibo",
        "partner_contact" : "Jamil Bhuiyan",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "385",
        "partner_name" : "Tesa",
        "partner_contact" : "Marco Bornhoeft",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "386",
        "partner_name" : "Tesa",
        "partner_contact" : "Martin Homeier",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "387",
        "partner_name" : "Tesa",
        "partner_contact" : "Fabian Krüger-Herbert",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "388",
        "partner_name" : "Tesa",
        "partner_contact" : "Daniela-Christin Meyers",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "389",
        "partner_name" : "Telekom",
        "partner_contact" : "Ulrike Schmidt von Knobelsdorf",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "390",
        "partner_name" : "Tetra Pak",
        "partner_contact" : "Anke Hampel",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "391",
        "partner_name" : "The Hackett Group",
        "partner_contact" : "Leopold Hermann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "392",
        "partner_name" : "Titan",
        "partner_contact" : "Oliver Engelbrecht",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "393",
        "partner_name" : "Titan",
        "partner_contact" : "Jakob Glaßmeier",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "394",
        "partner_name" : "TOI TOI & DIXI Sanitärsysteme",
        "partner_contact" : "Heiko Paskowski",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "395",
        "partner_name" : "Toi Toi Dixi Group",
        "partner_contact" : "Oliver Elsner",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "396",
        "partner_name" : "TX Group",
        "partner_contact" : "Leslie Weiss",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "397",
        "partner_name" : "U.I. Lapp",
        "partner_contact" : "Peer Grabowski",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "398",
        "partner_name" : "U.I. Lapp",
        "partner_contact" : "Peter Halbauer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "399",
        "partner_name" : "U.I. Lapp",
        "partner_contact" : "Ulrich Steinmill",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "400",
        "partner_name" : "Uwe Werner IT Einkaufsberatung",
        "partner_contact" : "Wolfgang Stangier",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "401",
        "partner_name" : "Uwe Werner IT Einkaufsberatung",
        "partner_contact" : "Uwe Werner",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "402",
        "partner_name" : "VEMAG Maschinenbau",
        "partner_contact" : "Florian Greulich",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "403",
        "partner_name" : "VEMAG Maschinenbau",
        "partner_contact" : "Steen Bösche",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "404",
        "partner_name" : "Visteron Corporation",
        "partner_contact" : "Sascha Zagorac",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "405",
        "partner_name" : "Wego Systembaustoffe",
        "partner_contact" : "Jens Bauder",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "406",
        "partner_name" : "Wego Systembaustoffe",
        "partner_contact" : "Christian Polstermüller",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "407",
        "partner_name" : "Wego Systembaustoffe",
        "partner_contact" : "Stjepan Tudek",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "408",
        "partner_name" : "Weru",
        "partner_contact" : "Rungas Markus",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "409",
        "partner_name" : "Wessel Hydraulik",
        "partner_contact" : "Malte Barkmann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "410",
        "partner_name" : "Witt & Sohn",
        "partner_contact" : "Roman Trembacz",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "411",
        "partner_name" : "World Bank Group",
        "partner_contact" : "Aleksandar Medjedovic",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "412",
        "partner_name" : "Wüstenrot",
        "partner_contact" : "Dirk Masch",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "413",
        "partner_name" : "WUCATO Marketplace",
        "partner_contact" : "",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "414",
        "partner_name" : "Xella Baustoffe",
        "partner_contact" : "Lennart Henny",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "415",
        "partner_name" : "Xella Baustoffe",
        "partner_contact" : "Daniel Marczinkowsky",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "416",
        "partner_name" : "Xt Supply",
        "partner_contact" : "Dominik Hecker",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "417",
        "partner_name" : "Zalando",
        "partner_contact" : "Alejandro Basterrechea",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "418",
        "partner_name" : "zb Zentralbahn",
        "partner_contact" : "Thomas Emmenegger",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
];

export default data