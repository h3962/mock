let data = [
    {
        "id" : "0.8",
        "partner_name" : "Daimler",
        "partner_contact" : "Raphael Jakob",
        "location" : "Hamburg",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "1",
        "partner_name" : "REWE",
        "partner_contact" : "Jonas Gelb",
        "location" : "Köln",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "2",
        "partner_name" : "REWE",
        "partner_contact" : "Oliver Goldowski",
        "location" : "Köln",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "3",
        "partner_name" : "REWE",
        "partner_contact" : "Anja Herges",
        "location" : "Köln",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "4",
        "partner_name" : "Volkswagen",
        "partner_contact" : "Wilfried Rode",
        "location" : "Braunschweig",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "5",
        "partner_name" : "LIDL",
        "partner_contact" : "Daniel Fahr",
        "location" : "Neckarsulm",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "6",
        "partner_name" : "LIDL",
        "partner_contact" : "Ron Gero Kayser",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "7",
        "partner_name" : "DELL",
        "partner_contact" : "Matthias Bux",
        "location" : "Wien",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "8",
        "partner_name" : "ZF Friedrichshafen",
        "partner_contact" : "Pierre Lindemann",
        "location" : "Wedel",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "9",
        "partner_name" : "BASF",
        "partner_contact" : "Dipl.-Ing. Philipp Wenz",
        "location" : "Ludwigshafen",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "10",
        "partner_name" : "Lufthansa Group",
        "partner_contact" : "Claudia Wilhelm",
        "location" : "Frankfurt",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "11",
        "partner_name" : "Samsung",
        "partner_contact" : "Markus Kyeck",
        "location" : "Köln/Bonn",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "12",
        "partner_name" : "SAP Deutschland",
        "partner_contact" : "Bernhard Straka",
        "location" : "Hamburg",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "13",
        "partner_name" : "SAP Deutschland",
        "partner_contact" : "Peer Meinhardt",
        "location" : "Stuttgart",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "14",
        "partner_name" : "SAP Deutschland",
        "partner_contact" : "Christian Suelzer",
        "location" : "Walldorf",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "15",
        "partner_name" : "Cisco Germany",
        "partner_contact" : "Johanna Ahrens",
        "location" : "Hamburg",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "16",
        "partner_name" : "Maersk Deutschland",
        "partner_contact" : "Torsten Pansegrau",
        "location" : "Hamburg",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "17",
        "partner_name" : "Bureau Veritas Germany",
        "partner_contact" : "Birol Yosul",
        "location" : "Hamburg",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "18",
        "partner_name" : "American Express",
        "partner_contact" : "Alexander Forca",
        "location" : "Frankfurt",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "19",
        "partner_name" : "Cisco",
        "partner_contact" : "Stefan Lein",
        "location" : "Sehnde",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "20",
        "partner_name" : "AstraZeneca",
        "partner_contact" : "Dr. Florian Altenberend",
        "location" : "Hamburg",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "21",
        "partner_name" : "Salesforce",
        "partner_contact" : "Tim Holm",
        "location" : "Hamburg",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "22",
        "partner_name" : "Salesforce",
        "partner_contact" : "Sylvia Huttanus",
        "location" : "Hamburg",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "23",
        "partner_name" : "Randstad Deutschland",
        "partner_contact" : "Sandra Thörner",
        "location" : "Essen",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "24",
        "partner_name" : "Randstad Deutschland",
        "partner_contact" : "Arjan Schorfhaar",
        "location" : "Frankfurt",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "25",
        "partner_name" : "British American Tobacco",
        "partner_contact" : "Andreas Thoma",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "26",
        "partner_name" : "Deutsche Telekom",
        "partner_contact" : "Michael Kimberger",
        "location" : "Bonn",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "27",
        "partner_name" : "Deutsche Telekom",
        "partner_contact" : "Metin Aydin",
        "location" : "Bonn",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "28",
        "partner_name" : "Otto",
        "partner_contact" : "Claudia Nölken-Niebuhr",
        "location" : "Hamburg",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "29",
        "partner_name" : "Otto",
        "partner_contact" : "Fabienne Gieb",
        "location" : "Hamburg",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "30",
        "partner_name" : "Carlsberg Deutschland",
        "partner_contact" : "Sabrina Welzel",
        "location" : "Hamburg",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "31",
        "partner_name" : "ALDI Nord",
        "partner_contact" : "Gu Young Kang",
        "location" : "Düsseldorf",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "32",
        "partner_name" : "ALDI Nord",
        "partner_contact" : "Dominik Plicht",
        "location" : "Bochum",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "33",
        "partner_name" : "Stahl-Armaturen PERSTA",
        "partner_contact" : "Matthias Lux",
        "location" : "Warstein",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "34",
        "partner_name" : "Honda Financial Services",
        "partner_contact" : "Volker Boehme",
        "location" : "Frankfurt",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "35",
        "partner_name" : "Kellogg",
        "partner_contact" : "Christoph Sterkel",
        "location" : "Hamburg",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "36",
        "partner_name" : "ERGO",
        "partner_contact" : "Steffen Keller",
        "location" : "Berlin/Brandenburg",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "37",
        "partner_name" : "ERGO",
        "partner_contact" : "Mathias Klein",
        "location" : "Berlin/Brandenburg",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "38",
        "partner_name" : "ERGO",
        "partner_contact" : "Stephan Naskowiak",
        "location" : "Düsseldorf",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "39",
        "partner_name" : "ERGO",
        "partner_contact" : "Mario Schewemann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "40",
        "partner_name" : "ERGO",
        "partner_contact" : "Ralf Schwarz",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "41",
        "partner_name" : "FC St. Pauli",
        "partner_contact" : "Martin Geisthardt",
        "location" : "Hamburg",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "42",
        "partner_name" : "TÜV SÜD",
        "partner_contact" : "Robert Kees",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "43",
        "partner_name" : "HDI Group",
        "partner_contact" : "Lisa-Marie Thumm",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "44",
        "partner_name" : "Fielmann",
        "partner_contact" : "Clemens Schneider",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "45",
        "partner_name" : "Securitas",
        "partner_contact" : "Thomas Otto",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "46",
        "partner_name" : "Securitas",
        "partner_contact" : "Marco Trupp",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "47",
        "partner_name" : "Douglas",
        "partner_contact" : "Sasan Ariatabar",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "48",
        "partner_name" : "Spie GmbH",
        "partner_contact" : "Oliver Kosche",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "49",
        "partner_name" : "Vattenfall",
        "partner_contact" : "Ina Vögele",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "50",
        "partner_name" : "Vattenfall",
        "partner_contact" : "Karsten Kranewitz",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "51",
        "partner_name" : "Jungheinrich PROFISHOP",
        "partner_contact" : "Nikolas Nissle",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "52",
        "partner_name" : "Jungheinrich Vertrieb Deutschland",
        "partner_contact" : "Benjamin Martens",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "53",
        "partner_name" : "PHOENIX CONTACT Deutschland",
        "partner_contact" : "Detlef Kloke",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "54",
        "partner_name" : "PHOENIX CONTACT Deutschland",
        "partner_contact" : "Stephan Volgmann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "55",
        "partner_name" : "VELUX Deutschland",
        "partner_contact" : "Christian Krüger",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "56",
        "partner_name" : "Vodafone",
        "partner_contact" : "Florian Wehmann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "57",
        "partner_name" : "Wanhua Chemical Group",
        "partner_contact" : "Gauthier Herrmann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "58",
        "partner_name" : "Adecco Business Solutions",
        "partner_contact" : "Kirsten Schönau",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "59",
        "partner_name" : "SPORTFIVE",
        "partner_contact" : "Thorsten Rittersberger",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "60",
        "partner_name" : "Vorwerk Deutschland Stiftung",
        "partner_contact" : "Swen Liebig",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "61",
        "partner_name" : "Ströer",
        "partner_contact" : "Jeniffer Käther",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "62",
        "partner_name" : "Ströer",
        "partner_contact" : "Puya Kelid-Dari",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "63",
        "partner_name" : "Ströer SE",
        "partner_contact" : "Laura von Kessel",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "64",
        "partner_name" : "AVL Deutschland",
        "partner_contact" : "Alexander Toppelreiter",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "65",
        "partner_name" : "AVL Deutschland",
        "partner_contact" : "Andreas Boiselle",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "66",
        "partner_name" : "AVL Deutschland",
        "partner_contact" : "Christian Paul",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "67",
        "partner_name" : "AVL Deutschland",
        "partner_contact" : "Viktoria Woskoboinik",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "68",
        "partner_name" : "AVL Deutschland",
        "partner_contact" : "Moritz Frobenius",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "69",
        "partner_name" : "AVL Deutschland",
        "partner_contact" : "Stefan Schmid",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "70",
        "partner_name" : "CWS-boco Deutschland",
        "partner_contact" : "Felix Reidelbach",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "71",
        "partner_name" : "Expleo",
        "partner_contact" : "Johannes Schwanzer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "72",
        "partner_name" : "Edenred",
        "partner_contact" : "André dos Santos",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "73",
        "partner_name" : "TÜV NORD Mobilität",
        "partner_contact" : "Dirk Helmold",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "74",
        "partner_name" : "Eberspächer Gruppe",
        "partner_contact" : "Christian Brosig",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "75",
        "partner_name" : "ElringKlinger",
        "partner_contact" : "Boris Wuthnow",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "76",
        "partner_name" : "Cooper Tire & Rubber Company",
        "partner_contact" : "Michael Lutz",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "77",
        "partner_name" : "Bosch Security Systems",
        "partner_contact" : "Dirk Schneider",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "78",
        "partner_name" : "EWE",
        "partner_contact" : "Dipl.-Ing. Oliver Bolay",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "79",
        "partner_name" : "Vodafone Business",
        "partner_contact" : "Maximilian Tritschler",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "80",
        "partner_name" : "DATEV",
        "partner_contact" : "Hans Martin Bauer",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "81",
        "partner_name" : "DATEV",
        "partner_contact" : "Jan Bielefeld",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "82",
        "partner_name" : "DATEV",
        "partner_contact" : "Christian Neuser",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "83",
        "partner_name" : "DATEV",
        "partner_contact" : "Christian Wenzel-Hofmann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "84",
        "partner_name" : "Rentokil Initial",
        "partner_contact" : "Torsten Baranowski",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "85",
        "partner_name" : "Adolf Würth",
        "partner_contact" : "André Gröbner",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "86",
        "partner_name" : "Adolf Würth",
        "partner_contact" : "Harald Kaßold",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "87",
        "partner_name" : "TARGOBANK",
        "partner_contact" : "Mounir Areslan",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "88",
        "partner_name" : "Würth",
        "partner_contact" : "Toni Birkigt",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "89",
        "partner_name" : "Würth Elektronik",
        "partner_contact" : "Heiko Arnold",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "90",
        "partner_name" : "Xella Deutschland",
        "partner_contact" : "Valentina Ikstadt",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "91",
        "partner_name" : "Xella Deutschland",
        "partner_contact" : "Grit Lochmann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "92",
        "partner_name" : "Xella Deutschland",
        "partner_contact" : "Pascal Wanner",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "93",
        "partner_name" : "Xella Deutschland",
        "partner_contact" : "Simone Wunsch",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "94",
        "partner_name" : "GOLDBECK",
        "partner_contact" : "Dipl.-Ing. Jörg Flüß",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "95",
        "partner_name" : "flaschenpost",
        "partner_contact" : "André G. Krell",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "96",
        "partner_name" : "HOMAG",
        "partner_contact" : "Stephan Holzner",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "97",
        "partner_name" : "Intuitive",
        "partner_contact" : "Tim Heisterkamp",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "98",
        "partner_name" : "HubSpot",
        "partner_contact" : "Kathleen Jaedtke",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "99",
        "partner_name" : "HubSpot",
        "partner_contact" : "Gregor Hufenreuter",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "100",
        "partner_name" : "HubSpot",
        "partner_contact" : "Maximilian Keil",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "101",
        "partner_name" : "Ferchau",
        "partner_contact" : "Steven Weinberg",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "102",
        "partner_name" : "DFDS Germany",
        "partner_contact" : "Niels Johansen",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "103",
        "partner_name" : "EDAG",
        "partner_contact" : "Dipl.-Ing. Sven Jacob",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "104",
        "partner_name" : "Ruhr-Universität Bochum",
        "partner_contact" : "Christian Schmitz",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "105",
        "partner_name" : "Ruhr-Universität Bochum",
        "partner_contact" : "Jan Wieseke",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "106",
        "partner_name" : "Wackler Holding",
        "partner_contact" : "Willi Fottner",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "107",
        "partner_name" : "Europcar",
        "partner_contact" : "Philipp Eickhoff",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "108",
        "partner_name" : "Immobilien Service Deutschland",
        "partner_contact" : "Jörn Wittenberg",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "109",
        "partner_name" : "Miba Sinter Holding",
        "partner_contact" : "Robert Gavran",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "110",
        "partner_name" : "SIXT",
        "partner_contact" : "Peter Bohrmann",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "111",
        "partner_name" : "DLL Financial Solutions Partner",
        "partner_contact" : "Steffen Schöck",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "112",
        "partner_name" : "MEWA Unternehmensgruppe",
        "partner_contact" : "Michael Kümpfel",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "113",
        "partner_name" : "MEWA Unternehmensgruppe",
        "partner_contact" : "Patrick Müller",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "114",
        "partner_name" : "ENGIE Deutschland",
        "partner_contact" : "Manuel Jörger",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "115",
        "partner_name" : "Lekkerland",
        "partner_contact" : "Cengiz Sevim",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },
    {
        "id" : "116",
        "partner_name" : "ebm-papst Group",
        "partner_contact" : "Patrick Christleven",
        "location" : "–",
        "data_quality" : "0.8",
        "partner_step" : "1"
    },

    {
        "id" : "117",
        "partner_name" : "Müller AG",
        "partner_contact" : "Max Mustermann",
        "location" : "–",
        "data_quality" : "0.4",
        "partner_step" : "4"
    },
    {
        "id" : "118",
        "partner_name" : "Walmart",
        "partner_contact" : "George Miller",
        "location" : "–",
        "data_quality" : "0.9",
        "partner_step" : "8"
    },
    {
        "id" : "119",
        "partner_name" : "Petrobas",
        "partner_contact" : "Juan Campuzano",
        "location" : "–",
        "data_quality" : "0.3",
        "partner_step" : "2"
    },
    {
        "id" : "120",
        "partner_name" : "Suco Inc.",
        "partner_contact" : "Stephan James",
        "location" : "–",
        "data_quality" : "0.7",
        "partner_step" : "6"
    },
];

export default data