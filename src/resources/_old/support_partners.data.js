let data = [
  {
    "id": 0,
    "partner_name": "EMOLTRA",
    "partner_step": "1",
    "currentPartnerInformation": {
    "headlineSub": "Kentucky • Produktion",
    "headline": "EMOLTRA",
    "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
    "actionHeadline": "Warten auf Hochladen der Referenzdaten",
    "sublineLeft": "Schritt 2/8",
    "sublineRight": "Start Anfrage: 01.02.2022",
    "button1": "Partner zu Aktion auffordern",
    "button1Icon": "fas fa-hand-point-right",
    "button2": "Videokonferenz mit Partner vereinbaren",
    "button2Icon": "fas fa-tv",
    "button3": "Prozess abbrechen",
    "button3Icon": "fas fa-ban",
    "progress": 0,
    "bottomPart": [
      {
        "key": "Partner nutzt bereits H&F Transformer",
        "value": "Ja"
      },
      {
        "key": "Letzte Aktion des Partners",
        "value": "02.02.2022"
      }
    ]
  }
  },
  {
    "id": 1,
    "partner_name": "SNOWPOKE",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Illinois • Produktion",
        "headline": "SNOWPOKE",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Warten auf Hochladen der Referenzdaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "NEIN"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 2,
    "partner_name": "WRAPTURE",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Florida • Produktion",
        "headline": "WRAPTURE",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Vervollständigung der Profildaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 3,
    "partner_name": "CALLFLEX",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Mississippi • Produktion",
        "headline": "CALLFLEX",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Ergänzung",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 4,
    "partner_name": "PRISMATIC",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Michigan • Produktion",
        "headline": "PRISMATIC",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Registrierung ausstehend",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 5,
    "partner_name": "JOVIOLD",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Maryland • Produktion",
        "headline": "JOVIOLD",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Vervollständigung der Profildaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "NEIN"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 6,
    "partner_name": "ZILLACOM",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Tennessee • Produktion",
        "headline": "ZILLACOM",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Warten auf Hochladen der Referenzdaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 7,
    "partner_name": "XURBAN",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Alabama • Produktion",
        "headline": "XURBAN",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Ergänzung",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "NEIN"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 8,
    "partner_name": "LUNCHPOD",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "New Mexico • Produktion",
        "headline": "LUNCHPOD",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Registrierung ausstehend",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "NEIN"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 9,
    "partner_name": "ENDICIL",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Delaware • Produktion",
        "headline": "ENDICIL",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Warten auf Hochladen der Referenzdaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 10,
    "partner_name": "ORBIXTAR",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Vermont • Produktion",
        "headline": "ORBIXTAR",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Warten auf Hochladen der Referenzdaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 11,
    "partner_name": "MEDIOT",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Arkansas • Produktion",
        "headline": "MEDIOT",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Ergänzung",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 12,
    "partner_name": "MUSIX",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Indiana • Produktion",
        "headline": "MUSIX",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Warten auf Hochladen der Referenzdaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "NEIN"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 13,
    "partner_name": "CINASTER",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Guam • Produktion",
        "headline": "CINASTER",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Warten auf Hochladen der Referenzdaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 14,
    "partner_name": "SHADEASE",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Nebraska • Produktion",
        "headline": "SHADEASE",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Ergänzung",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 15,
    "partner_name": "MICROLUXE",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "New York • Produktion",
        "headline": "MICROLUXE",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Ergänzung",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "NEIN"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 16,
    "partner_name": "PULZE",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Missouri • Produktion",
        "headline": "PULZE",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Registrierung ausstehend",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "NEIN"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 17,
    "partner_name": "CALCULA",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "New Hampshire • Produktion",
        "headline": "CALCULA",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Warten auf Hochladen der Referenzdaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "NEIN"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 18,
    "partner_name": "BULLJUICE",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Oklahoma • Produktion",
        "headline": "BULLJUICE",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Registrierung ausstehend",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 19,
    "partner_name": "CUJO",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "South Carolina • Produktion",
        "headline": "CUJO",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Vervollständigung der Profildaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 20,
    "partner_name": "TELPOD",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Arizona • Produktion",
        "headline": "TELPOD",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Registrierung ausstehend",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 21,
    "partner_name": "FUELTON",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "South Dakota • Produktion",
        "headline": "FUELTON",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Vervollständigung der Profildaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 22,
    "partner_name": "BIOTICA",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Wisconsin • Produktion",
        "headline": "BIOTICA",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Vervollständigung der Profildaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "NEIN"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 23,
    "partner_name": "ESCHOIR",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Georgia • Produktion",
        "headline": "ESCHOIR",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Vervollständigung der Profildaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "NEIN"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 24,
    "partner_name": "QIAO",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Idaho • Produktion",
        "headline": "QIAO",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Vervollständigung der Profildaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 25,
    "partner_name": "SATIANCE",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Texas • Produktion",
        "headline": "SATIANCE",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Registrierung ausstehend",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 26,
    "partner_name": "ROCKLOGIC",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "American Samoa • Produktion",
        "headline": "ROCKLOGIC",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Warten auf Hochladen der Referenzdaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 27,
    "partner_name": "JAMNATION",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Ohio • Produktion",
        "headline": "JAMNATION",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Registrierung ausstehend",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress":0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "NEIN"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 28,
    "partner_name": "EXODOC",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Louisiana • Produktion",
        "headline": "EXODOC",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Ergänzung",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 29,
    "partner_name": "PHORMULA",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Kansas • Produktion",
        "headline": "PHORMULA",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Registrierung ausstehend",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 30,
    "partner_name": "TERASCAPE",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Puerto Rico • Produktion",
        "headline": "TERASCAPE",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Warten auf Hochladen der Referenzdaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "NEIN"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 31,
    "partner_name": "QUANTASIS",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Oregon • Produktion",
        "headline": "QUANTASIS",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Ergänzung",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 10,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 32,
    "partner_name": "ZYPLE",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Maine • Produktion",
        "headline": "ZYPLE",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Ergänzung",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "NEIN"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 33,
    "partner_name": "EARTHPLEX",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Marshall Islands • Produktion",
        "headline": "EARTHPLEX",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Registrierung ausstehend",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "NEIN"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 34,
    "partner_name": "ZENTIME",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Montana • Produktion",
        "headline": "ZENTIME",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Vervollständigung der Profildaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "NEIN"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 35,
    "partner_name": "TROPOLI",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Northern Mariana Islands • Produktion",
        "headline": "TROPOLI",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Vervollständigung der Profildaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 36,
    "partner_name": "COMCUBINE",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Virgin Islands • Produktion",
        "headline": "COMCUBINE",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Ergänzung",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "NEIN"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 37,
    "partner_name": "THREDZ",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Palau • Produktion",
        "headline": "THREDZ",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Vervollständigung der Profildaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "NEIN"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 38,
    "partner_name": "ELEMANTRA",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Federated States Of Micronesia • Produktion",
        "headline": "ELEMANTRA",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Registrierung ausstehend",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 39,
    "partner_name": "OLYMPIX",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Utah • Produktion",
        "headline": "OLYMPIX",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Warten auf Hochladen der Referenzdaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "NEIN"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 40,
    "partner_name": "FITCORE",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Colorado • Produktion",
        "headline": "FITCORE",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Warten auf Hochladen der Referenzdaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 41,
    "partner_name": "ECOSYS",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "West Virginia • Produktion",
        "headline": "ECOSYS",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Warten auf Hochladen der Referenzdaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "NEIN"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 42,
    "partner_name": "ZYTREX",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Washington • Produktion",
        "headline": "ZYTREX",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Registrierung ausstehend",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 43,
    "partner_name": "ARTWORLDS",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "New Jersey • Produktion",
        "headline": "ARTWORLDS",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Vervollständigung der Profildaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "NEIN"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 44,
    "partner_name": "PERMADYNE",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Massachusetts • Produktion",
        "headline": "PERMADYNE",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Registrierung ausstehend",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 45,
    "partner_name": "GOLISTIC",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Nevada • Produktion",
        "headline": "GOLISTIC",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Ergänzung",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 46,
    "partner_name": "ISOTERNIA",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Hawaii • Produktion",
        "headline": "ISOTERNIA",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Vervollständigung der Profildaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "NEIN"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 47,
    "partner_name": "BRISTO",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Wyoming • Produktion",
        "headline": "BRISTO",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Warten auf Hochladen der Referenzdaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 48,
    "partner_name": "ENDIPIN",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "District Of Columbia • Produktion",
        "headline": "ENDIPIN",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Warten auf Hochladen der Referenzdaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 49,
    "partner_name": "NIKUDA",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Iowa • Produktion",
        "headline": "NIKUDA",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Ergänzung",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "NEIN"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 50,
    "partner_name": "ISOSTREAM",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "North Carolina • Produktion",
        "headline": "ISOSTREAM",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Vervollständigung der Profildaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 10,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 51,
    "partner_name": "VALPREAL",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Rhode Island • Produktion",
        "headline": "VALPREAL",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Warten auf Hochladen der Referenzdaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress":0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 52,
    "partner_name": "SNORUS",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Minnesota • Produktion",
        "headline": "SNORUS",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Ergänzung",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "NEIN"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 53,
    "partner_name": "MULTRON",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Pennsylvania • Produktion",
        "headline": "MULTRON",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Registrierung ausstehend",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress":0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 54,
    "partner_name": "GENMY",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Alaska • Produktion",
        "headline": "GENMY",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Registrierung ausstehend",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 55,
    "partner_name": "INTERODEO",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Connecticut • Produktion",
        "headline": "INTERODEO",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Vervollständigung der Profildaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "NEIN"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 56,
    "partner_name": "ACCEL",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "California • Produktion",
        "headline": "ACCEL",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Registrierung ausstehend",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 57,
    "partner_name": "SULTRAXIN",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Virginia • Produktion",
        "headline": "SULTRAXIN",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Ergänzung",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "NEIN"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 58,
    "partner_name": "ZILPHUR",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Kentucky • Produktion",
        "headline": "ZILPHUR",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Registrierung ausstehend",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 59,
    "partner_name": "GENESYNK",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Illinois • Produktion",
        "headline": "GENESYNK",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Vervollständigung der Profildaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 60,
    "partner_name": "NETROPIC",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Florida • Produktion",
        "headline": "NETROPIC",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Vervollständigung der Profildaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "NEIN"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 61,
    "partner_name": "SULFAX",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Mississippi • Produktion",
        "headline": "SULFAX",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Registrierung ausstehend",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 62,
    "partner_name": "COMDOM",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Michigan • Produktion",
        "headline": "COMDOM",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Vervollständigung der Profildaten",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "Ja"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  },
  {
    "id": 63,
    "partner_name": "MOMENTIA",
    "partner_step": "1",
    "currentPartnerInformation": {
        "headlineSub": "Maryland • Produktion",
        "headline": "MOMENTIA",
        "actionHeadlineSub": "Aktueller Bearbeitungsschritt",
        "actionHeadline": "Ergänzung",
        "sublineLeft": "Schritt 2/8",
        "sublineRight": "Start Anfrage: 01.02.2022",
        "button1": "Partner zu Aktion auffordern",
        "button1Icon": "fas fa-hand-point-right",
        "button2": "Videokonferenz mit Partner vereinbaren",
        "button2Icon": "fas fa-tv",
        "button3": "Prozess abbrechen",
        "button3Icon": "fas fa-ban",
        "progress": 0,
        "bottomPart": [
          {
            "key": "Partner nutzt bereits H&F Transformer",
            "value": "NEIN"
          },
          {
            "key": "Letzte Aktion des Partners",
            "value": "02.02.2022"
          }
        ]
      }
  }
]

export default data