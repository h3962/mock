let data = [
  {
    "id": 0,
    "partner_name": "EMOLTRA",
    "partner_contact": "info@emoltra.com",
    "data_quality": 0.9646705126966537
  },
  {
    "id": 1,
    "partner_name": "SNOWPOKE",
    "partner_contact": "info@snowpoke.com",
    "data_quality": 0.8888096262150793
  },
  {
    "id": 2,
    "partner_name": "WRAPTURE",
    "partner_contact": "info@wrapture.com",
    "data_quality": 0.02773464582462437
  },
  {
    "id": 3,
    "partner_name": "CALLFLEX",
    "partner_contact": "info@callflex.com",
    "data_quality": 0.5011280052586335
  },
  {
    "id": 4,
    "partner_name": "PRISMATIC",
    "partner_contact": "info@prismatic.com",
    "data_quality": 0.57721925740149
  },
  {
    "id": 5,
    "partner_name": "JOVIOLD",
    "partner_contact": "info@joviold.com",
    "data_quality": 0.9652142397044854
  },
  {
    "id": 6,
    "partner_name": "ZILLACOM",
    "partner_contact": "info@zillacom.com",
    "data_quality": 0.4686569064601227
  },
  {
    "id": 7,
    "partner_name": "XURBAN",
    "partner_contact": "info@xurban.com",
    "data_quality": 0.7555358715093021
  },
  {
    "id": 8,
    "partner_name": "LUNCHPOD",
    "partner_contact": "info@lunchpod.com",
    "data_quality": 0.3027271013360473
  },
  {
    "id": 9,
    "partner_name": "ENDICIL",
    "partner_contact": "info@endicil.com",
    "data_quality": 0.32056413498766867
  },
  {
    "id": 10,
    "partner_name": "ORBIXTAR",
    "partner_contact": "info@orbixtar.com",
    "data_quality": 0.34510067441342596
  },
  {
    "id": 11,
    "partner_name": "MEDIOT",
    "partner_contact": "info@mediot.com",
    "data_quality": 0.5967395988028636
  },
  {
    "id": 12,
    "partner_name": "MUSIX",
    "partner_contact": "info@musix.com",
    "data_quality": 0.792417888647718
  },
  {
    "id": 13,
    "partner_name": "CINASTER",
    "partner_contact": "info@cinaster.com",
    "data_quality": 0.6655867064760199
  },
  {
    "id": 14,
    "partner_name": "SHADEASE",
    "partner_contact": "info@shadease.com",
    "data_quality": 0.8653185056713746
  },
  {
    "id": 15,
    "partner_name": "MICROLUXE",
    "partner_contact": "info@microluxe.com",
    "data_quality": 0.705086679850558
  },
  {
    "id": 16,
    "partner_name": "PULZE",
    "partner_contact": "info@pulze.com",
    "data_quality": 0.7888812576150532
  },
  {
    "id": 17,
    "partner_name": "CALCULA",
    "partner_contact": "info@calcula.com",
    "data_quality": 0.637238367618266
  },
  {
    "id": 18,
    "partner_name": "BULLJUICE",
    "partner_contact": "info@bulljuice.com",
    "data_quality": 0.7987716974065147
  },
  {
    "id": 19,
    "partner_name": "CUJO",
    "partner_contact": "info@cujo.com",
    "data_quality": 0.21471289995153486
  },
  {
    "id": 20,
    "partner_name": "TELPOD",
    "partner_contact": "info@telpod.com",
    "data_quality": 0.24510845737712272
  },
  {
    "id": 21,
    "partner_name": "FUELTON",
    "partner_contact": "info@fuelton.com",
    "data_quality": 0.22700157125066367
  },
  {
    "id": 22,
    "partner_name": "BIOTICA",
    "partner_contact": "info@biotica.com",
    "data_quality": 0.9228582274586641
  },
  {
    "id": 23,
    "partner_name": "ESCHOIR",
    "partner_contact": "info@eschoir.com",
    "data_quality": 0.9514562067604466
  },
  {
    "id": 24,
    "partner_name": "QIAO",
    "partner_contact": "info@qiao.com",
    "data_quality": 0.6395132226859999
  },
  {
    "id": 25,
    "partner_name": "SATIANCE",
    "partner_contact": "info@satiance.com",
    "data_quality": 0.12738888744416887
  },
  {
    "id": 26,
    "partner_name": "ROCKLOGIC",
    "partner_contact": "info@rocklogic.com",
    "data_quality": 0.41210409516182467
  },
  {
    "id": 27,
    "partner_name": "JAMNATION",
    "partner_contact": "info@jamnation.com",
    "data_quality": 0.9054466875994285
  },
  {
    "id": 28,
    "partner_name": "EXODOC",
    "partner_contact": "info@exodoc.com",
    "data_quality": 0.7951252782839768
  },
  {
    "id": 29,
    "partner_name": "PHORMULA",
    "partner_contact": "info@phormula.com",
    "data_quality": 0.5989032634963771
  },
  {
    "id": 30,
    "partner_name": "TERASCAPE",
    "partner_contact": "info@terascape.com",
    "data_quality": 0.14273589399557984
  },
  {
    "id": 31,
    "partner_name": "QUANTASIS",
    "partner_contact": "info@quantasis.com",
    "data_quality": 0.6420641064285157
  },
  {
    "id": 32,
    "partner_name": "ZYPLE",
    "partner_contact": "info@zyple.com",
    "data_quality": 0.8327689570798937
  },
  {
    "id": 33,
    "partner_name": "EARTHPLEX",
    "partner_contact": "info@earthplex.com",
    "data_quality": 0.09156815992755307
  },
  {
    "id": 34,
    "partner_name": "ZENTIME",
    "partner_contact": "info@zentime.com",
    "data_quality": 0.413450678619228
  },
  {
    "id": 35,
    "partner_name": "TROPOLI",
    "partner_contact": "info@tropoli.com",
    "data_quality": 0.2583310885950614
  },
  {
    "id": 36,
    "partner_name": "COMCUBINE",
    "partner_contact": "info@comcubine.com",
    "data_quality": 0.392123855454346
  },
  {
    "id": 37,
    "partner_name": "THREDZ",
    "partner_contact": "info@thredz.com",
    "data_quality": 0.7600588671937349
  },
  {
    "id": 38,
    "partner_name": "ELEMANTRA",
    "partner_contact": "info@elemantra.com",
    "data_quality": 0.13914734378061455
  },
  {
    "id": 39,
    "partner_name": "OLYMPIX",
    "partner_contact": "info@olympix.com",
    "data_quality": 0.3880928169644682
  },
  {
    "id": 40,
    "partner_name": "FITCORE",
    "partner_contact": "info@fitcore.com",
    "data_quality": 0.0024459661895614992
  },
  {
    "id": 41,
    "partner_name": "ECOSYS",
    "partner_contact": "info@ecosys.com",
    "data_quality": 0.4041335603965055
  },
  {
    "id": 42,
    "partner_name": "ZYTREX",
    "partner_contact": "info@zytrex.com",
    "data_quality": 0.04027085639611716
  },
  {
    "id": 43,
    "partner_name": "ARTWORLDS",
    "partner_contact": "info@artworlds.com",
    "data_quality": 0.11407094450540467
  },
  {
    "id": 44,
    "partner_name": "PERMADYNE",
    "partner_contact": "info@permadyne.com",
    "data_quality": 0.11192954138745081
  },
  {
    "id": 45,
    "partner_name": "GOLISTIC",
    "partner_contact": "info@golistic.com",
    "data_quality": 0.47868078719779406
  },
  {
    "id": 46,
    "partner_name": "ISOTERNIA",
    "partner_contact": "info@isoternia.com",
    "data_quality": 0.17396179337317697
  },
  {
    "id": 47,
    "partner_name": "BRISTO",
    "partner_contact": "info@bristo.com",
    "data_quality": 0.23021750754142323
  },
  {
    "id": 48,
    "partner_name": "ENDIPIN",
    "partner_contact": "info@endipin.com",
    "data_quality": 0.45483534135445103
  },
  {
    "id": 49,
    "partner_name": "NIKUDA",
    "partner_contact": "info@nikuda.com",
    "data_quality": 0.38064267483142844
  },
  {
    "id": 50,
    "partner_name": "ISOSTREAM",
    "partner_contact": "info@isostream.com",
    "data_quality": 0.9189307681496925
  },
  {
    "id": 51,
    "partner_name": "VALPREAL",
    "partner_contact": "info@valpreal.com",
    "data_quality": 0.8459417013239097
  },
  {
    "id": 52,
    "partner_name": "SNORUS",
    "partner_contact": "info@snorus.com",
    "data_quality": 0.25355642444925985
  },
  {
    "id": 53,
    "partner_name": "MULTRON",
    "partner_contact": "info@multron.com",
    "data_quality": 0.6790163767054198
  },
  {
    "id": 54,
    "partner_name": "GENMY",
    "partner_contact": "info@genmy.com",
    "data_quality": 0.5972321582254723
  },
  {
    "id": 55,
    "partner_name": "INTERODEO",
    "partner_contact": "info@interodeo.com",
    "data_quality": 0.8428811891774755
  },
  {
    "id": 56,
    "partner_name": "ACCEL",
    "partner_contact": "info@accel.com",
    "data_quality": 0.94982502603733638
  },
  {
    "id": 57,
    "partner_name": "SULTRAXIN",
    "partner_contact": "info@sultraxin.com",
    "data_quality": 0.1779198479230104
  },
  {
    "id": 58,
    "partner_name": "ZILPHUR",
    "partner_contact": "info@zilphur.com",
    "data_quality": 0.46824115330021554
  },
  {
    "id": 59,
    "partner_name": "GENESYNK",
    "partner_contact": "info@genesynk.com",
    "data_quality": 0.5646544447510982
  },
  {
    "id": 60,
    "partner_name": "NETROPIC",
    "partner_contact": "info@netropic.com",
    "data_quality": 0.28163226660929097
  },
  {
    "id": 61,
    "partner_name": "SULFAX",
    "partner_contact": "info@sulfax.com",
    "data_quality": 0.04492388064034847
  },
  {
    "id": 62,
    "partner_name": "COMDOM",
    "partner_contact": "info@comdom.com",
    "data_quality": 0.35939957182666427
  },
  {
    "id": 63,
    "partner_name": "MOMENTIA",
    "partner_contact": "info@momentia.com",
    "data_quality": 0.46546305746778693
  }
]



  export default data