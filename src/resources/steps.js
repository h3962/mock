let data = [
  {"id": 0, "name": "Partner importiert"},
  {"id": 1, "name": "Teilnehmer angefragt"},
  {"id": 2, "name": "Belege von Teilnehmer eingegangen"},
  {"id": 3, "name": "Belege von Teilnehmer geprüft und Datenqualität ausreichend"},
  {"id": 4, "name": "Teilnehmer zur Datenkorrektur aufgefordert"},
  {"id": 5, "name": "Teilnehmer wiederholt zur Datenkorrektur aufgefordert"},
  {"id": 6, "name": "Vernetzung abgebrochen"},
  {"id": 7, "name": "Vernetzung abgebrochen"},
  // {"id": 7, "name": "Verbindung aktiv"},
]

export default data