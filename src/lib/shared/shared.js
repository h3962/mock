import Swal from "sweetalert2";

export function fireToast(type, title) {
    Swal.fire({
        toast: true,
        icon: type,
        title: title,
        animation: false,
        position: 'bottom-left',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })
}
