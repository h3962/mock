import {writable} from "svelte/store";
import Swal from "sweetalert2";

// TODO: This is needed when input should be bind directly to an input with bind:value={...} <-- use this later
// export const registrationData = writable(localStorage.getItem('registrationData' || ''))
// registrationData.subscribe((val) => localStorage.setItem('registrationData', val))

export function updateRegistrationData(key, value)
{
    updateData('registrationData', key, value)
}

export function getRegistrationData(key) {
    return getData('registrationData', key)
}

export function storeRegistrationData()
{
    storeDataUnderDifferentName('registrationData')
}

export function updateCalculatorData(key, value)
{
    updateData('calculatorData', key, value)
}

export function getCalculatorData(key)
{
    return getData('calculatorData', key)
}

export function storeCalculatorData()
{
    storeDataUnderDifferentName('calculatorData')
}

/*
* ############################################
* GENERAL
* ############################################
* */
export function fieldGiven(value)
{
    return undefined !== value && null !== value && '' !== value
}

/**
 * Function takes a storageId
 * */
function getData(storageId, key = null)
{
    let registrationData = localStorage.getItem(storageId)
    if(null === key){
        return registrationData
    }
    let parsed = JSON.parse(registrationData)
    return parsed[key]
}

function getAllData(onlyInitial = false)
{
    if(!onlyInitial){
        console.log(localStorage)

        return localStorage
    }

    let registrationData = getData('registrationData')
    let calculatorData = getData('calculatorData')

    return [registrationData, calculatorData]
}

/**
 * Function takes a storageId
 * */
function updateData(storageId, key, value)
{
    let data = localStorage.getItem(storageId)
    if(null === data){
        localStorage.setItem(storageId, '{}')
    }

    let dataJson = {}
    try {
        dataJson = JSON.parse(data)
    } catch (e){}

    if(null === dataJson){
        dataJson = {}
    }

    dataJson[key] = value
    localStorage.setItem(storageId, JSON.stringify(dataJson))
}


/**
 * Takes the current data and stores it under another name.
 * Resets the other calculatorData.
 */
export function storeDataUnderDifferentName(storageId)
{
    let currentData = getData(storageId)
    let timestamp = Math.floor(Date.now() / 1000)
    let newId = storageId + '_' + (timestamp)
    localStorage.setItem(newId, currentData)
    console.log(currentData, timestamp, newId, localStorage.getItem(newId))
}

/**
 * Function to safe all data as file and reset.
 */
export function storeAll(filename)
{
    Swal.fire({
        title: 'Wie soll der Dateiname lauten?',
        html: '<input id="store-data-filename" placeholder="Dateiname"/>.json',
    }).then(function(e) {

        if(!e.isConfirmed) return;

        let fileName = jQuery('#store-data-filename').val()

        download(fileName + '.json', JSON.stringify(getAllData(), null, '\t'))

        localStorage.clear();
    });
}

function download(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}